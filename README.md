# 呼叫中心手机APP #

主要用于外勤人员和警务人员的手机端app.

### 创建手机app ###

* 首先安装[Node.js](https://nodejs.org/), [Cordova](http://cordova.apache.org), [Ant](http://ant.apache.org)
* 然后执行(安装对安卓的支持)： 
<br/>
<code>$ cordova platform add android </code>
* 安装插件（调试Console）<br />
<code>$ cordova plugin add org.apache.cordova.console</code>
* 安装gps插件:<br/>
<code>$ cordova plugin add org.apache.cordova.geolocation</code>
* 安装网络插件：<br />
<code>$ cordova plugin add org.apache.cordova.network-information</code>
* 安装二维码插件：<br />
<code>$ npm install -g plugman <br />
$ plugman install --platform android --project . --plugin https://github.com/wildabeast/BarcodeScanner
</code>
* 编译：<br/>
<code>$ cordova build</code>
* 等待编译完成，将 <br/>
<code>$ path/to/project/platforms/android/ant-build/MainActivity-debug.apk</code>
安装在安卓设备上。

